import React from 'react';
import InputSendComp from './components/input-send/input-send.comp';
import MessageComp from './components/messages/messages.comp';
import NavComp from './components/nav-buttons/nav-buttons.comp';

const {
    div
} = React.DOM;

export
default React.createClass({
    render() {
        const inputComp = React.createElement(InputSendComp, {}),
            messageComp = React.createElement(MessageComp, {
                msg: 'hello'
            }),
            navButtons = React.createElement(NavComp, {
                buttons: [{
                    label: 'play',
                    value: '1'
                }, {
                    label: 'stop',
                    value: '2'
                }]
            });

        return div({}, inputComp, messageComp, navButtons);
    }
});
