import React from 'react';

const {
    div,
    p
} = React.DOM;

export
default React.createClass({
	
	getDefaultProps() {
		return {
			msg:'',
			msgtype: ''
		};
	},

    render() {
        return div({}, p({}, this.props.msg));
    }
});
