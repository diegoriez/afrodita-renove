import React from 'react';
import utils from '../../sub/utilities';

const {
    div,
    input,
    button
} = React.DOM, {
    noop
} = utils;

export
default React.createClass({

    getDefaultProps() {
            return {
                parentCallback: noop,
                sendButtonLabel: 'send'
            };
        },

        onClickHandler() {
            const value = (React.findDOMNode(this.refs.inputMod).value);
            this.props.parentCallback(value);
        },

        render() {
            return div({}, input({
                type: 'text',
                ref: 'inputMod'
            }), button({
                type: 'button',
                onClick: this.onClickHandler
            }, this.props.sendButtonLabel));
        }
});
