import React from 'react';
import utils from '../../sub/utilities';
import R from 'ramda';

const {
    nav,
    button
} = React.DOM, {
    noop
} = utils;

export
default React.createClass({

    getDefaultProps() {
            return {
                parentCallback: noop,
                buttons: []
            };
        },

        onClickHandler(e) {
            const val = e.target.value;
            this.props.parentCallback(val);
        },

        render() {

            let inc = 0,
                buttons = R.map(item => {
                    return button({
                        key: `btn_${inc=R.inc(inc)}`,
                        type: 'button',
                        value: item.value,
                        onClick: this.onClickHandler
                    }, item.label);


                }, this.props.buttons);

            return nav({}, buttons);
        }
});
