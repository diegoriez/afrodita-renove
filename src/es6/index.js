import visualizer from './visualizer';
import React from 'react';
import ReactDOM from 'react-dom';

ReactDOM.render(
    React.createElement(visualizer, {}),
    document.querySelector('#container')
);
