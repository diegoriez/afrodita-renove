var src = './src',
    dist = './dist',
    build = './bundle';

module.exports = {

    dist: {
        dir: dist, // dit dest 
        scriptsDest: '/scripts'
    },

    build: {
        dir: build, // build directory
        bundleFileName: 'bundle.js', // default name, of the bundle file
        scriptsDir: build.concat('/scripts'),
        indexHTMLfileName: 'index.html'
    },

    main: src.concat('/es6/index.js'), //start entry point
    es5: src.concat('/es5'), // transpiled files
    es6: src.concat('/es6/**/*.js'), // modules

    clean: [(src.concat('/es5/*')), build, dist], //clean directories
    copy: [src.concat('/index.html')],
    lint: src.concat('/es6/**/*.js'),
    test: ['tape spec/*.js | tap-colorize',
        'tape spec/sub/**/*.js | tap-colorize',
        'tape spec/componets/**/*.js | tap-colorize'
    ],

    watch: {
        spec: './spec/**/*.js',
        build: [src.concat('/es6/**/*.js'), src.concat('/index.html')]
    },

    server: {
        baseDir: build
    }
};
