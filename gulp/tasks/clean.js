'use strict';

var gulp = require('gulp'),
    rimraf = require('gulp-rimraf'),
    paths = require('../paths-config');

// Clean
gulp.task('clean', function() {
    return gulp.src(paths.clean, {
        read: false
    }).pipe(rimraf());
});
