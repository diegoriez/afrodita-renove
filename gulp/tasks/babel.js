var gulp = require('gulp'),
    babel = require('gulp-babel'),
    sourcemaps = require('gulp-sourcemaps'),
    paths = require('../paths-config');

gulp.task('babel', function() {
    return gulp.src(paths.es6)
    	.pipe(sourcemaps.init())
        .pipe(babel())
    	.pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest(paths.es5));
});