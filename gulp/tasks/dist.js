'use strict';

var gulp = require('gulp'),
    runSequence = require('run-sequence'),
    uglify = require('gulp-uglify'),
    minifyHTML = require('gulp-minify-html'),
    paths = require('../paths-config'),

    compressJsFile = paths.build.scriptsDir.concat('/').concat(paths.build.bundleFileName),
    minifyHtmlFile = paths.build.dir.concat('/').concat(paths.build.indexHTMLfileName),
    distScriptsDest = paths.dist.dir.concat('/').concat(paths.dist.scriptsDest);

/*
|-------------------------------------------
| TASKS
|-------------------------------------------
*/

gulp.task('compress', function() {
    return gulp.src(compressJsFile)
        .pipe(uglify())
        .pipe(gulp.dest(distScriptsDest));
});

gulp.task('minify-html', function() {
    var opts = {
        conditionals: true,
        spare: true
    };

    return gulp.src(minifyHtmlFile)
        .pipe(minifyHTML(opts))
        .pipe(gulp.dest(paths.dist.dir));
});

gulp.task('dist', function() {
    runSequence(    'clean', 
                    'babel', 
                    ['lint', 'exec-tests'], 
                    'babelify',
                    'copy', 
                    'compress', 
                    'minify-html');
});
