'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    runSequence = require('run-sequence'),
    browserSync = require('browser-sync').create(),
    paths = require('../paths-config');

// Watch
gulp.task('watch', function() {

    gulp.watch(paths.watch.spec, ['exec-tests']);
    gulp.watch(paths.watch.build, function() {
        runSequence(
            'clean', 'babel', ['exec-tests', 'lint'], 'babelify', 'copy', browserSync.reload
        )
    });
});

gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: paths.server.baseDir
        },
        ui: {
            port: 4000
        },
        reloadDelay: 1500
    });
});