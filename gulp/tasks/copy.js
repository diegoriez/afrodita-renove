var gulp = require('gulp'),
    paths = require('../paths-config');

gulp.task('copy', function() {
    return gulp.src(paths.copy)
        // Perform minification tasks, etc here
        .pipe(gulp.dest(paths.build.dir));
});