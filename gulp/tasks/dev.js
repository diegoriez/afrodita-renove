'use strict';

var gulp = require('gulp'),
    runSequence = require('run-sequence');

gulp.task('dev', function() {
    runSequence('clean', 
    		'babel', 
    		['exec-tests', 'lint'], 
    		'babelify',
    		'copy',
    		'browser-sync',
    		'watch');
});