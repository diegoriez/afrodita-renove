var gulp = require('gulp'),
    babelify = require('babelify'),
    source = require('vinyl-source-stream'),
    browserify = require('browserify'),
    paths = require('../paths-config');

gulp.task('babelify', function() {
    return browserify({entries:paths.main, debug: true})
    		.transform(babelify, {presets: ['es2015']})
    		.bundle()
  			.pipe(source(paths.build.bundleFileName))
  			.pipe(gulp.dest(paths.build.scriptsDir));
});
