# README #

Simply to start a ES6 project + gulp + Browserify + react 

* Start a ES6 project. It's modules will be converted to ES5 using Babeljs.
* Web-project

### How do I get set up? ###

* clone this repository, git clone https://diegoriez@bitbucket.org/diegoriez/seedproject.git
* npm install
* gulp dev
* gulp task dist to generate a dist compressed version 