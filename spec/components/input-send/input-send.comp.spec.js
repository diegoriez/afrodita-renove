var test = require('tape'),
    React = require('react'),
    sd = require('skin-deep'),
    inputSend = require('../../../src/es5/components/input-send/input-send.comp').default,

    before = test,
    after = test,
    tree,
    vdom,
    instance,
    $ = React.createElement;

before('desc:', function(t) {
    t.pass('input component. A div element with an inpunt and send button');
    t.end();
});

before('exist', function(t) {
    t.ok(React, 'react lib is correctly imported');
    t.ok(test, 'test lib correctly imported');
    t.ok(sd, 'skin-deep lib correctly imported');
    t.ok(inputSend, 'react component correctly imported');
    t.end();
});

before('should render a react element', function(t) {

    var comp = $(inputSend, {}),
        expectedChildren,
        actual, expect;

    tree = sd.shallowRender(comp);
    vdom = tree.getRenderOutput();
    instance = tree.getMountedInstance();

    expectedChildren = [
        React.DOM.input({
            type: 'text',
            ref: 'inputMod'
        }),
        React.DOM.button({
            type: 'button',
            onClick: instance.onClickHandler
        }, 'send')
    ];

    actual = vdom.type;
    expect = 'div';
    t.equal(actual, expect, 'should be a div element');

    actual = vdom.props.children;
    expect = expectedChildren;
    t.deepEqual(actual, expect, 'children, should contain an input and button');

    t.end();
});

before('intance', function(t) {

    var actual, expect;

    actual = typeof instance.props.parentCallback;
    expect = 'function';
    t.equal(actual, expect, 'should have a predefined callback function');

    actual = instance.props.sendButtonLabel;
    expect = 'send';
    t.equal(actual, expect, 'sendButtonLabel, should have a predefined value');

    t.end();
});

test('onClickHandler', function(t) {
    var actual, expect;
    actual = typeof instance.onClickHandler;
    expect = 'function';
    t.equal(actual, expect, 'onClickHandler, should be a function');
    t.end();
});


test('end', function(t){
    t.pass('//------------------------------------------------//');
    t.end()
});