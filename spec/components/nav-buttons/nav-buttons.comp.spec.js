var test = require('tape'),
    React = require('react'),
    sd = require('skin-deep'),
    navs = require('../../../src/es5/components/nav-buttons/nav-buttons.comp').default,

    before = test,
    after = test,
    tree,
    vdom,
    instance,
    $ = React.createElement;

before('desc:', function(t) {
    t.pass('render a nav buttons bar');
    t.end();
});

before('exist', function(t) {
    t.ok(React, 'react lib is correctly imported');
    t.ok(test, 'test lib correctly imported');
    t.ok(sd, 'skin-deep lib correctly imported');
    t.ok(navs, 'react component correctly imported');
    t.end();
});

before('should render a react element', function(t) {

    var comp = $(navs, {
            'buttons': [{
                label: 'play',
                value: '1'
            }, {
                label: 'stop',
                value: '2'
            }]
        }),
        actual, expect;

    tree = sd.shallowRender(comp);
    vdom = tree.getRenderOutput();
    instance = tree.getMountedInstance();

    actual = vdom.type;
    expect = 'nav';
    t.equal(actual, expect, 'should be a nav element');

    expectedChildren = [
        React.DOM.button({
            type: 'button',
            value:'1',
            onClick: instance.onClickHandler,
            key: 'btn_1'
        }, 'play'),
        React.DOM.button({
            type: 'button',
            value:'2',
            onClick: instance.onClickHandler,
            key: 'btn_2'
        }, 'stop')
    ];

    actual = vdom.props.children;
    expect = expectedChildren;
    t.deepEqual(actual, expect, '["play", "stop"], should render nav buttons childrens');

    t.end();
});

before('instance properties', function(t) {
    var actual, expect;
    actual = typeof instance.props.parentCallback;
    expect = 'function';
    t.equal(actual, expect, 'default parentCallback function shoud be defined');
    t.end();

});

test('end', function(t) {
    t.pass('//------------------------------------------------//');
    t.end()
});