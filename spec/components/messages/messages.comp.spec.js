var test = require('tape'),
    React = require('react'),
    sd = require('skin-deep'),
    messages = require('../../../src/es5/components/messages/messages.comp').default,

    before = test,
    after = test,
    tree,
    vdom,
    instance,
    $ = React.createElement;

before('desc:', function(t) {
    t.pass('render a messages component');
    t.end();
});

before('exist', function(t) {
    t.ok(React, 'react lib is correctly imported');
    t.ok(test, 'test lib correctly imported');
    t.ok(sd, 'skin-deep lib correctly imported');
    t.ok(messages, 'react component correctly imported');
    t.end();
});

before('should render a react element', function(t) {

    var comp = $(messages, {}),
        actual, expect, expectedChildren;

    tree = sd.shallowRender(comp);
    vdom = tree.getRenderOutput();
    instance = tree.getMountedInstance();

    actual = vdom.type;
    expect = 'div';
    t.equal(actual, expect, 'should be a div element');

    expectedChildren = React.DOM.p({}, '');

    actual = vdom.props.children;
    expect = expectedChildren;
    t.deepEqual(actual, expect, 'child, should be "<p>" element');

    t.end();
});

before('instance properties', function(t) {
    var actual, expect;
    actual = instance.props.msg;
    expect = '';
    t.equal(actual, expect, 'initial msg prop, should be  "" ');

    actual = instance.props.msgtype;
    expect = '';
    t.equal(actual, expect, 'initial msgtype type prop, should be  "" ');

    t.end();
});


test('end', function(t){
    t.pass('//------------------------------------------------//');
    t.end()
});