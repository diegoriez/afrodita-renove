var test = require('tape'),
    React = require('react'),
    sd = require('skin-deep'),
    visualizer = require('../src/es5/visualizer').default,

    before = test,
    after = test,
    tree,
    vdom,
    instance,
    $ = React.createElement;

before('desc:', function(t) {
    t.pass('render a visualizer, the container of components');
    t.end();
});

before('exist', function(t) {
    t.ok(React, 'react lib is correctly imported');
    t.ok(test, 'test lib correctly imported');
    t.ok(sd, 'skin-deep lib correctly imported');
    t.ok(visualizer, 'react visualizer component correctly imported');
    t.end();
});

before('should render visualizer', function(t) {

    var comp = $(visualizer, {}),
        actual, expect, expectedChildren;

    tree = sd.shallowRender(comp);
    vdom = tree.getRenderOutput();
    instance = tree.getMountedInstance();

    actual = vdom.type;
    expect = 'div';
    t.equal(actual, expect, 'should be a div element');

    t.end();
});
